const YTDL = require("ytdl-core");

YTDL.idToURL = function(id)
{
	return "https://www.youtube.com/watch?v=" + id;
}

YTDL.durToSecs = function(dur)
{
	// Drop "PT".
	dur = dur.substr(2).toLowerCase();
	
	hInd = dur.indexOf('h');
	mInd = dur.indexOf('m');
	sInd = dur.indexOf('s');
	
	var secs = 0;
	
	if (hInd != -1)
	{
		secs += parseInt(dur.substr(0, hInd), 10)*60*60;
	}
	
	if (mInd != -1)
	{
		// If there's no hour marker hInd+1 is 0.
		secs += parseInt(dur.substr(hInd+1, mInd), 10)*60;
	}
	
	// No seconds is 0S, no hours or mins means the substr is from 0, otherwise it's from whichever exists
	// or is the furthest along (mins if both exist).
	
	secs += parseInt(dur.substr(Math.max(hInd, mInd)+1, sInd), 10);
	
	return secs;
}

module.exports = YTDL;