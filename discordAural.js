const Discord = require("discord.js");

const bot = new Discord.Client();
Discord.getBot = () => {return bot;}

Discord.getNick = function(member)
{
	if (!member)
	{
		return "";
	}
	
	if (member.nickname)
	{
		return member.nickname;
	}
	else
	{
		return member.user.username;
	}
}

Discord.getGuild = function()
{
	return bot.guilds.first(1)[0];
}

// Better than saving the names and changing on event, as this won't track changes when the bot is off.
Discord.getUserName = function(userId)
{
	const member = Discord.getGuild().members.get(userId);
	
	if (!member)
	{
		return;
	}
	
	return Discord.getNick(member);
}

module.exports = Discord;