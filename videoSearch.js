const YTSearch = require("node-yt-search");
const helpers = require("./helpers");

const initVideoSearch = function(Video, youTubeKey)
{
	const videoSearch = function(term, resultNum, msg, callBack)
	{
		const opts =
		{
			key: youTubeKey,
			type: "video",
			maxResults: resultNum
		}
		
		YTSearch(term, opts, (err, result) => searchResult(err, result));
		
		function searchResult(err, result)
		{
			if (err)
			{
				helpers.logErr(err);
				return;
			}
			
			const searchVideos = result.items;
			
			if (!searchVideos || searchVideos.length == 0)
			{
				if (!searchVideos)
				{
					console.log("Search failed: " + err)
					console.trace();
				}
				
				msg.channel.send("No videos found.");
				
				return;
			}
			
			const videos = [];
			
			// for (var searchVideo in results) has a null searchVideo for some reason.
			for (var i = 0; i < searchVideos.length; ++i)
			{
				videos.push(Video.fromSearchVideo(searchVideos[i], msg.author.id));
			}
			
			callBack(videos, msg);
		}
	}
	
	return videoSearch;
}

module.exports = initVideoSearch;