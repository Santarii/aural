const YTDL = require("./ytdlAural");
const helpers = require("./helpers");

const initPlayer = function(Video, videoSearch, Queue)
{
	const Player = function(inQueue)
	{
		const self = this;
		const queue = inQueue;
		const voteRatioReq = 0.75;
		var dispatcher;
		var connectionStarted = false;
		var connection;
		var currVideo;
		var skipVotes = [];
		var currStream;
		const multiStreams = [];
		var multiTimer;
		
		self.searchAndAddSong = async function(input)
		{
			var searchTerm = input.comInput;
			const msg = input.msg;
			
			if (!searchTerm)
			{
				msg.channel.send("No search term provided.");
				return;
			}
			
			if (YTDL.validateURL(searchTerm))
			{
				const video = await Video.fromId(YTDL.getVideoID(searchTerm), msg.author.id);
				self.addSong(video, msg);
				return;
			}
			
			videoSearch(searchTerm, 1, msg, (videos, msg) => self.addSong(videos[0], msg));
		}
		
		self.play = function()
		{
			if (currVideo.part > 1)
			{
				currStream = multiStreams[currVideo.adderId.toString()];
				
				if (!currStream)
				{
					throw new Error("Multi-part video stream not found.");
				}
				
				if (currVideo.part == currVideo.parts)
				{
					multiStreams[currVideo.adderId] = null;
				}
				
				dispatcher = connection.playStream(currStream, {seek: currVideo.getStartSecs()});
			}
			else
			{
				currStream = YTDL
				(
					currVideo.url,
					{
						filter: "audioonly",
						quality: "highestaudio"
					}
				);
				
				dispatcher = connection.playStream(currStream);
				
				if (currVideo.isMulti())
				{
					multiStreams[currVideo.adderId.toString()] = currStream;
				}
				
				//currStream.on("progress", ytdlProgress);
			}
			
			// If the video part is the last part, just let it play to the end.
			if (currVideo.isMulti() && currVideo.part != currVideo.parts)
			{
				multiTimer = setInterval(checkMultiLength, 1000);
			}
			
			dispatcher.on("end", playNext);
			
			console.log("Trying to play song: " + currVideo.url + "\n");
		}
		
		self.voteSkip = function(input)
		{
			if (!connection)
			{
				return;
			}
			
			const msg = input.msg;
			const msgChannel = msg.channel;
			const userId = msg.author.id;
			
			// If member not in correct voice channel, return.
			if (!connection.channel.members.has(userId))
			{
				return;
			}
			
			if (skipVotes.includes(userId))
			{
				msgChannel.send(msg.author.username + " already voted to skip.");
				return;
			}
			
			skipVotes.push(userId);
			
			const currVotes = skipVotes.length;
			const voteReq = getVoteReq();
			
			if (currVotes >= voteReq)
			{
				const voteReq = getVoteReq();
				msgChannel.send(voteReq + "/" + voteReq + " votes.  Skipping song.");
				
				skip();
				return;
			}
			
			msg.channel.send(currVotes + "/" + voteReq + " votes to skip.");
		}
		
		self.forceSkip = function(input)
		{
			if (!connection)
			{
				return;
			}
			
			input.msg.channel.send("Skipping song.");
			
			skip();
		}
		
		self.activate = function(input)
		{
			queue.setActivate(input, true);
			
			checkJoin(input.msg);
		}
		
		self.deactivate = function(input)
		{
			queue.setActivate(input, false);
		}
		
		self.current = function(input)
		{
			if (currVideo)
			{
				currVideo.sendEmbed("Current Video", input.msg);
			}
		}
		
		self.addSong = function(video, msg)
		{
			// Adds song to queue and displays video embed.
			queue.addSong(video, msg);
			checkJoin(msg);
		}
		
		/*function ytdlProgress(chunkLen, totDownloaded, totDownloadLen)
		{
			var downloadedAmount = (totDownloaded/totDownloadLen);
			
			if (downloadedAmount > currVideo.getEndProg())
			{
				currStream.destroy();
				console.log("Downloaded full amount of video part.");
			}
		*/
		
		function checkMultiLength()
		{
			if (dispatcher.time/1000 > currVideo.getPartLengthSecs())
			{
				clearInterval(multiTimer);
				multiTimer = null;
				dispatcher.end();
			}
		}
		
		function skip()
		{
			if (!dispatcher)
			{
				return;
			}
			
			console.log("Skipping song.");
				
			// This already handles playing the next song.
			dispatcher.end();
		}
		
		function getVoteReq()
		{
			const members = connection.channel.members;
			
			// Max of member.size-2, take 1 to account for the bot in there, take another to account for whose song it is.
			return Math.max(Math.ceil((members.size-2)*voteRatioReq), 1);
		}
		
		function playNext()
		{
			skipVotes = [];
			currVideo = null;
			currStream = null;
			
			if (multiTimer)
			{
				clearInterval(multiTimer);
				multiTimer = null;
			}
			
			if (!connection)
			{
				console.log("Connection became " + connection);
				return;
			}
			
			currVideo = queue.nextUser(connection.channel);
			
			if (currVideo)
			{
				try
				{
					self.play();
				}
				catch (err)
				{
					helpers.logErr(err);
					playNext();
				}
			}
			else
			{
				// Disconnect.
				dispatcher = null;
				connection.channel.leave();
				connection.disconnect();
				connectionStarted = false;
				connection = null;
				console.log("Left voice.");
			}
		}
		
		function checkJoin(msg)
		{
			// If bot is not voice connected and the member is and the member is valid (has a song and is set to activate).
			if (!connectionStarted && msg.member.voiceChannel && queue.isValidUser(msg.author.id))
			{
				// Cannot check if connection is null as it is set on a delay, fast song requests could cause multiple connections.
				connectionStarted = true;
				msg.member.voiceChannel.join()
					.then((newConnection) => {connection = newConnection; playNext();})
					.catch(helpers.logErr);
			}
		}
	}
	
	return Player;
}

module.exports = initPlayer;