const helpers = require("./helpers");
const FS = require("fs");
const Discord = require("./discordAural");

const config = JSON.parse(FS.readFileSync("config.json"));

const InputHandler = require("./inputHandler");

const inputHandler = new InputHandler(config);
const bot = Discord.getBot();

function onReady()
{
	console.log("Logged in as " + bot.user.tag + ".\n");
	inputHandler.comConfigRoles();
}

bot.on("ready", onReady);
bot.on("message", inputHandler.onMessage);

bot.login(config.discordToken);