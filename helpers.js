const helpers = () => {};

Array.prototype.rotateR = function()
{
	const self = this;
	self.unshift(self.pop());
}

Number.prototype.pad = function(finalLength)
{
	var s = this.toString();
	
	while (s.length < finalLength)
	{
		s = "0" + s;
	}
	
	return s;
}

helpers.noOp = () => {};

helpers.logErr = function(err)
{
	console.log(err);
	console.trace();
}

helpers.getDigits = function(num)
{
	// 0 will result in 1 digit;
	return Math.max(1, Math.floor(1+Math.log10(num)));
}

module.exports = helpers;