const FS = require("fs");
const Discord = require("./discordAural");
const embedConfig = require("./embedConfig");

initQueue = function(Video)
{
	const Queue = function(setDir)
	{
		const self = this;
		const dir = setDir;
		var data =
		{
			users: {},
			activeUsers: [],
			newUsers: [],
			inactiveUsers: []
		}
		
		init();
		
		self.ensureUser = function(member)
		{
			// If a new user.
			if (!data.users[member.id])
			{
				console.log("Adding new user.");
				data.users[member.id] = {activate: true, songs: []};
				data.newUsers.unshift(member.id);
				saveQueue();
			}
		}
		
		self.addSong = function(video, msg)
		{
			const userId = msg.author.id;
			self.ensureUser(msg.member);
			
			data.users[userId].songs.unshift(video);
			video.sendEmbed("Added", msg, data.users[userId].songs.length);
			console.log("Adding a song.")
			saveQueue();
		}
		
		// Puts next song as current song.  Pops video if there is now a song ready to be played.
		self.nextUser = function(channel)
		{
			// Rotate active users, leaves top (right) available for insertion.
			if (data.activeUsers.length > 1)
			{
				console.log("Rotating active users.");
				data.activeUsers.rotateR();
				saveQueue();
			}
			
			return nextUserLoop(channel);
		}
		
		self.isValidUser = function(userId)
		{
			var user = data.users[userId];
			return user.activate && (user.songs.length > 0);
		}
		
		self.setActivate = function(input, activate)
		{
			const msg = input.msg;
			const userId = msg.author.id;
			self.ensureUser(msg.member);
			
			if (data.users[userId].activate != activate)
			{
				data.users[userId].activate = activate;
				msg.channel.send(msg.author.username + " has " + (activate ? "" : "de") + "activated their queue.");
				console.log("User activate set to " + activate + ".");
				saveQueue();
			}
			else
			{
				msg.channel.send(msg.author.username + "'s queue is already " + (activate ? "" : "de") + "activated.");
			}
		}
		
		self.sendUserQueueEmbed = function(input)
		{
			const maxLength = 50;
			const songsPerPage = 12;
			const msg = input.msg;
			const channel = msg.channel;
			const member = msg.member;
			const userQueue = data.users[member.id] ? data.users[member.id].songs : [];
			const pages = Math.max(1, (Math.ceil(userQueue.length/songsPerPage)));
			const parsedInput = parseInt(input.comInput, 10);
			const page = parsedInput ? Math.min(pages, Math.max(1, parsedInput)) : 1;		// Will be 1 if the parseInt fails (NaN);
			const startQNum = 1 + ((page-1)*songsPerPage);
			const endQNum = Math.min(userQueue.length, startQNum + (songsPerPage-1));
			
			try
			{
				const queueEmbed = new Discord.RichEmbed()
					.setColor(embedConfig.col)
					.setAuthor(embedConfig.restrictLength(Discord.getNick(member) + "'s Queue", maxLength), member.user.avatarURL)
					.setDescription(embedConfig.restrictLength("***Page " + page + "/" + pages + "***", maxLength, true));
				
				for (var qNum = startQNum; qNum <= endQNum; ++qNum)
				{
					var i = qNumIndexSwap(qNum, userQueue.length);
					var vid = Video.fromJSON(userQueue[i]);
					
					Video.addVideoField(queueEmbed, qNum, endQNum, vid);
				}
				
				input.msg.channel.send(queueEmbed);
			}
			catch (err)
			{
				console.log("Couldn't send queue embed: " + err);
			}
		}
		
		self.removeSong = function(input)
		{
			const msg = input.msg;
			const channel = msg.channel;
			const userId = msg.author.id;
			const comInput = input.comInput;
			
			if (!data.users[userId] || data.users[userId].songs.length == 0)
			{
				channel.send("No songs queued.");
				return;
			}
			
			const songs = data.users[userId].songs;
			
			if (!comInput)
			{
				channel.send("Provide a queue number to remove a song from your queue.");
				return;
			}
			
			const removeQNum = parseInt(comInput, 10);
			
			// NaN check doesn't work despite this being NaN when a letter is input.
			if (!removeQNum && removeQNum !== 0)
			{
				channel.send("Invalid queue number.");
				return;
			}
			
			if (removeQNum < 1)
			{
				channel.send("Provide a number over 0.");
				return;
			}
			
			if (removeQNum > songs.length)
			{
				channel.send("Provide a number less than your queue length (" + songs.length + ").");
				return;
			}
			
			removedJSONVid = songs.splice(qNumIndexSwap(removeQNum, songs.length), 1)[0];
			saveQueue();
			
			// Send video info embed of removed song.
			Video.fromJSON(removedJSONVid).sendEmbed("Removed", msg);
		}
		
		self.clearQueue = function(input)
		{
			const msg = input.msg;
			const userId = msg.author.id;
			
			if (data.users[userId])
			{
				data.users[userId].songs = [];
				saveQueue();
			}
			
			msg.channel.send("Queue cleared.");
		}
		
		self.removeLast = function(input)
		{
			const msg = input.msg;
			const qUser = data.users[msg.author.id];
			
			if (qUser && qUser.songs.length > 0)
			{
				removedJSONVid = qUser.songs.shift();
				Video.fromJSON(removedJSONVid).sendEmbed("Removed", msg);
				saveQueue();
			}
		}
		
		self.moveSong = function(input)
		{
			const msg = input.msg;
			const channel = msg.channel;
			const qUser = data.users[msg.author.id];
			const provideInputsMsg = "Provide an origin number and a destination number to move a song.";
			var comInput = input.comInput;
			
			if (!qUser || qUser.songs.length == 0)
			{
				channel.send("No songs queued.");
				return;
			}
			
			const songs = qUser.songs;
			
			if (!comInput)
			{
				channel.send(provideInputsMsg);
				return;
			}
			
			const comInputs = comInput.split(" ");
			const toStr = comInputs.pop();
			const fromStr = comInputs.pop();
			
			if (!(fromStr && toStr))
			{
				channel.send(provideInputsMsg);
				return;
			}
			
			function strToIndexWithMessages(str, name)
			{
				const num = parseInt(str, 10);
				
				if (!num && num !== 0)
				{
					channel.send("Invalid " + name + " number.");
					return -1;
				}
				
				if (num < 1)
				{
					channel.send("The " + name + " number is too small.");
					return -1;
				}
				
				if (num > songs.length)
				{
					channel.send("The " + name + " number is too large (max " + songs.length + ").");
					return -1;
				}
				
				return qNumIndexSwap(num, songs.length);
			}
			
			const fromIndex = strToIndexWithMessages(fromStr, "origin");
			const toIndex = strToIndexWithMessages(toStr, "destination");
			
			if (fromIndex == -1 || toIndex == -1)
			{
				return;
			}
			
			var movedJSONVid;
			
			if (fromIndex == toIndex)
			{
				movedJSONVid = songs[fromIndex];
			}
			else
			{
				movedJSONVid = songs.splice(fromIndex, 1)[0];
				songs.splice(toIndex, 0, movedJSONVid);
				saveQueue();
			}
			
			Video.fromJSON(movedJSONVid).sendEmbed("Moved", msg);
			
		}
		
		// Convers from queue num to index and the other way around.
		function qNumIndexSwap(qNumIndex, qLength)
		{
			return qLength - qNumIndex;
		}
		
		function popSong()
		{
			console.log("Popping song.");
			const video = Video.fromJSON(data.users[currUserId()].songs.pop());
			
			// Multi-part videos not currently working as intended, loads from beginning of song.
			
			/*if (video.isMulti() && video.part < video.parts)
			{
				// Need to get new copy instead of incrementing the part so as to not change the returned video.
				data.users[currUserId()].songs.push(video.getNext());
			}*/
			
			saveQueue();
			return video;
		}
		
		function saveQueue()
		{
			FS.writeFileSync(dir, JSON.stringify(data, null, 4), {flag: "w"});
		}
		
		function init()
		{
			var contents;
			try
			{
				contents = FS.readFileSync(dir);
				data = JSON.parse(contents);
			}
			catch (err)
			{
				// File does not exist.  Make file.
				console.log("Queue file did not exist: " + err);
				saveQueue();
			}
		}
		
		// Loops until valid user with song is placed at the top of activeUsers (pops video) or returns null.
		function nextUserLoop(channel)
		{
			var userId;
			
			// If there is a new user set to become active, move them from top of newUsers to top of activeUsers.
			for (var i = data.newUsers.length-1; i >= 0; --i)
			{
				userId = data.newUsers[i];
				
				if (isPlayableUser(userId, channel))
				{
					data.activeUsers.push(userId);
					data.newUsers.splice(i, 1);
					console.log("New user set as curr.");
					return popSong();
				}
			}
			
			// If there is an inactive user set to become active, move them from top of newUsers to top of activeUsers.
			for (var i = data.inactiveUsers.length-1; i >= 0; --i)
			{
				userId = data.inactiveUsers[i];
				
				if (isPlayableUser(userId, channel))
				{
					data.activeUsers.push(userId);
					data.inactiveUsers.splice(i, 1);
					console.log("Inactive user set as curr.");
					return popSong();
				}
			}
			
			// If no more active users, return null.
			if (data.activeUsers.length == 0)
			{
				console.log("No valid users.");
				return null;
			}
			
			// If user set to become inactive or has no songs, move from activeUsers to bottom of inactiveUsers.
			if (!isPlayableUser(currUserId(), channel))
			{
				data.inactiveUsers.unshift(data.activeUsers.pop());
				saveQueue();
				
				console.log("Deactivating user.");
				
				// Further checks for the next in list or new users or inactive users.
				return nextUserLoop(channel);
			}
			
			console.log("Next active in queue was valid.");
			
			return popSong();
		}
		
		function currUserId()
		{
			return data.activeUsers[data.activeUsers.length-1];
		}
		
		function isPlayableUser(userId, channel)
		{
			return self.isValidUser(userId) && channel.members.has(userId);
		}
	}
	
	return Queue;
}

module.exports = initQueue;