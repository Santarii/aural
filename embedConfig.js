const embedConfig = new (function()
{
	const self = this;
	self.col = 0xa50000;
	const markDownCodes = ["*", "\`"];
	
	self.restrictLength = function(str, maxLength, ignoreMarkDown)
	{
		if (str.indexOf("\n") != -1)
		{
			const parts = str.split("\n");
			str = "";
			
			var i = 0;
			while (true)
			{
				str += self.restrictLength(parts[i], maxLength, ignoreMarkDown);
				
				if (++i >= parts.length)
				{
					break;
				}
				
				str += "\n";
			}
			
			return str;
		}
		
		if (ignoreMarkDown)
		{
			maxLength + self.countMarkDownCodes(str);
		}
		
		if (str.length > maxLength)
		{
			return str.substr(0, maxLength-3) + "...";
		}
		
		return str;
	}
	
	self.countMarkDownCodes = function(str)
	{
		var count = 0;
		
		for (var code in markDownCodes)
		{
			var step = code.length;
			var pos = 0;
			
			while (true)
			{
				pos = str.indexOf(code, pos)
				
				if (pos < 0)
				{
					break;
				}
				
				++count;
				pos += step;
			}
		}
		
		return count;
	}
})();

module.exports = embedConfig;