const helpers = require("./helpers");
const Discord = require("./discordAural");
const YTDL = require("./ytdlAural");
const embedConfig = require("./embedConfig");

const initVideo = function(configMaxVideoLengthSecs)
{
	const defMaxVideoLengthSecs = 8*60;
	const maxVideoLengthSecs = configMaxVideoLengthSecs ? configMaxVideoLengthSecs : defMaxVideoLengthSecs;
	const maxFieldLength = 35;
	const multiPaddingSecs = 5;
	
	const Video = function(url, title, thumbURL, durSecs, adderId)
	{
		const self = this;
		self.url = url;
		self.title = title;
		self.thumbURL = thumbURL;
		self.durSecs = durSecs;
		self.adderId = adderId;
		self.part = 1;
		self.parts = Math.ceil(self.durSecs/maxVideoLengthSecs);
		
		self.isMulti = function()
		{
			return self.parts > 1;
		}
		
		self.getFullTitle = function()
		{
			return (self.isMulti() ? ("(" + self.part + "/" + self.parts + ") ") : "") + self.title;
		}
		
		// Has padding, rewinds a little when going to the next section.
		self.getStartSecs = function()
		{
			return Math.max(0, (self.durSecs*(self.part-1)/self.parts)-multiPaddingSecs);
		}
		
		self.getPartLengthSecs = function()
		{
			if (self.part > 1)
			{
				return multiPaddingSecs + (self.durSecs/self.parts);
			}
			
			return (self.durSecs/self.parts);
		}
		
		self.sendEmbed = function(action, msg, qPos)
		{
			try
			{
				const videoEmbed = new Discord.RichEmbed()
					.setColor(embedConfig.col)
					.setAuthor(embedConfig.restrictLength(action, maxFieldLength))
					.setThumbnail(self.thumbURL)
					.setTitle(embedConfig.restrictLength(self.getFullTitle(), maxFieldLength))
					.setURL(self.url)
					.setDescription(embedConfig.restrictLength("\`" + self.getTimeCode() + "\`\nAdded by: *" + Discord.getUserName(self.adderId) + "*", maxFieldLength, true));
				
				if (qPos)
				{
					videoEmbed.setFooter("Queue Position: " + qPos);
				}
				
				msg.channel.send(videoEmbed);
			}
			catch (err)
			{
				console.log("Couldn't send video embed: " + err);
				console.trace();
			}
		}
		
		self.getTimeCode = function()
		{
			var timeCode = (self.durSecs % 60).pad(2);
			
			const minutes = self.durSecs / 60;
			const hours = minutes / 60;
			
			// Don't check if minutes is over 1, always display the minutes even if it is 00.
			timeCode = Math.floor(minutes).pad(2) + ":" + timeCode;
			
			if (hours >= 1)
			{
				timeCode = Math.floor(hours).pad(2) + ":" + timeCode;
			}
			
			return timeCode;
		}
		
		self.getNext = function()
		{
			const newVid = new Video(self.url, self.title, self.thumbURL, self.durSecs, self.adderId);
			newVid.part = self.part+1;
			
			return newVid;
		}
	}
	
	Video.fromJSON = function(jsonVid)
	{
		const newVid = new Video(jsonVid.url, jsonVid.title, jsonVid.thumbURL, jsonVid.durSecs, jsonVid.adderId);
		newVid.part = jsonVid.part;
		newVid.parts = jsonVid.parts;
		return newVid;
	}

	Video.fromSearchVideo = function(searchVideo, adderId)
	{
		return new Video(
			YTDL.idToURL(searchVideo.id),
			searchVideo.snippet.title,
			searchVideo.snippet.thumbnails.default.url,
			YTDL.durToSecs(searchVideo.contentDetails.duration), adderId);
	}

	Video.fromId = async function(vidId, adderId)
	{
		var info = await YTDL.getInfo(vidId).catch(helpers.logErr);
		
		// The thumbnail is the default thumbnail.
		return new Video(info.video_url, info.title, info.thumbnail_url, info.length_seconds, adderId);
	}

	Video.addVideoField = function(listEmbed, currListNum, endListNum, vid)
	{
		const maxDigits = helpers.getDigits(endListNum);
		const digits = helpers.getDigits(currListNum);
		const spaces = "\u2002".repeat(maxDigits-digits);
		
		return listEmbed.addField("\`" + vid.getTimeCode() + "\`", "\`" + currListNum + ":\`\u2002" + spaces + "[" + vid.getFullTitle() + "](" + vid.url + ")");
	}
	
	return Video;
}

module.exports = initVideo;