# Aural

---

## Description

---

A Discord music bot that rotates between users.  Users who have never had a song played get top priority, users who have been skipped due to not having any songs left, leaving a channel, or having set themselves to deactivate get priority after new users, otherwise the queue just rotates between users.

Users can add as many songs to their personal queue as they want without preventing others from listening to songs.

Whoever has gone the most songs without having their song played will always have their song played next.

New feature progress: https://trello.com/b/IRmL9NKy/aural

## Commands

---

### ***Play***
**Aliases:** `!play`, `!p`, `!add`, `!addtoqueue`

**Description:** Provide a search term (or YouTube video URL).  Adds a YouTube video to your personal queue, Aural will join your voice channel if it is not in a voice channel.  If you put on a video over the max length, it will only play part of it, this feature is bugged at the moment (it should play play it in multiple parts so that it doesn't take up the queue for others), this will be fixed soon.

---

### ***Search***
**Aliases:** `!search`, `!s`, `!find`, `!f`

**Description:** Provide a search term, displays a list of up to 12 results from youtube.  Type the number for the song you want to add it to your queue, or type anything other than a number to cancel the search.  Times out after 30 seconds.

---

### ***Vote Skip***
**Aliases:** `!voteskip`, `!skip`, `!vs`, `!votenext`, `!next`, `!vn`

**Description:** Vote to skip the current song, requires at least 75% of the users (rounded up), and at most all users but one.

---

### ***Force Skip***
**Aliases:** `!forceskip`, `!fs`, `!forcenext`, `!fn`

**Description:** Anyone with a moderator role or higher can use this to skip a song.

---

### ***Activate***
**Aliases:** `!activate`, `!a`, `!join`

**Description:** Sets your queue to activate, your songs will be played.  Aural will join your voice channel if it is not in a voice channel and you have a song queued.

---

### ***Deactivate***
**Aliases:** `!deactivate`, `!d`, `!da`, `!leave`

**Description:** Sets your queue to deactivate, your songs won't be played until you activate your queue.  This won't affect any currently playing song.

---

### ***Current***
**Aliases:** `!current`, `!curr`, `!c`, `!nowplaying`, `!np`, `!this`

**Description:** Displays what song is currently playing, provides the title, thumbnail, link, and who added the song.

---

### ***Queue***
**Aliases:** `!queue`, `!q`, `!myqueue`, `!mylist`, `!list`, `!l`

**Description:** Displays your personal song queue.  Provide a number to view a specific page of your queue.

---

### ***Remove***
**Aliases:** `!remove`, `!rm`, `!r`, `!delete`, `!del`

**Description:** Provide a number, removes that song from your queue (use the Queue command to see the song numbers in your queue).

---

### ***Clear Queue***
**Aliases:** `!clearqueue`, `!clear`, `!cq`, `!clearall`, `!removeall`, `!ra`, `!deleteall`, `!delall`

**Description:** Clears your entire queue.

---

### ***Remove Last***
**Aliases:** `!removelast`, `!rl`

**Description:** Removes the last song from your queue.

---

### ***Move***
**Aliases:** `!move`, `!m`, `!put`

**Description:** Provide an origin and destiniation number, moves the song's position in your queue from the origin to the destination.