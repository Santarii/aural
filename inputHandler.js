const Discord = require("./discordAural");
const embedConfig = require("./embedConfig");

// Stores the text that follows the command and the Discord message the command was in.
const Input = function(comInput, msg)
{
	const self = this;
	self.comInput = comInput;
	self.msg = msg;
}

const InputHandler = new (function()
{
	const anyRole = -1;
	const commandPrefix = "!";
	
	return function(config)
	{
		const self = this;
		
		const Video = require("./video")(config.maxVideoLengthSecs);
		const videoSearch = require("./videoSearch")(Video, config.youTubeKey);
		const Queue = require("./queue")(Video);
		const Player = require("./player")(Video, videoSearch, Queue);
		
		const queueDir = "queue.json";
		const queue = new Queue(queueDir);
		const player = new Player(queue);
		
		var openQueries = {};
		const coms =
		{
			play:		{aliases: ["play", "p", "add", "addtoqueue"], func: player.searchAndAddSong, getMinRole: getDefRole},
			search:		{aliases: ["search", "s", "find", "f"], func: comSearch, getMinRole: getDefRole},
			voteSkip:	{aliases: ["voteskip", "skip", "vs", "votenext", "next", "vn"], func: player.voteSkip, getMinRole: getDefRole},
			forceSkip:	{aliases: ["forceskip", "fs", "forcenext", "fn"], func: player.forceSkip, getMinRole: getHighestRole},
			activate:	{aliases: ["activate", "a", "join"], func: player.activate, getMinRole: getDefRole},
			deactivate:	{aliases: ["deactivate", "d", "da", "leave"], func: player.deactivate, getMinRole: getDefRole},
			current:	{aliases: ["current", "curr", "c", "nowplaying", "np", "this"], func: player.current, getMinRole: getDefRole},
			queue:		{aliases: ["queue", "q", "myqueue", "mylist", "list", "l"], func: queue.sendUserQueueEmbed, getMinRole: getDefRole},
			remove:		{aliases: ["remove", "rm", "r", "delete", "del"], func: queue.removeSong, getMinRole: getDefRole},
			clearQueue:	{aliases: ["clearqueue", "clear", "cq", "clearall", "removeall", "ra", "deleteall", "delall"], func: queue.clearQueue, getMinRole: getDefRole},
			removeLast:	{aliases: ["removelast", "rl"], func: queue.removeLast, getMinRole: getDefRole},
			move:		{aliases: ["move", "m", "put"], func: queue.moveSong, getMinRole: getDefRole}
		};
		
		self.comConfigRoles = function()
		{
			// Check if there are any minRoles for the functions in the config.
			const minRoles = config.minRoles;
			for (var comKey in coms)
			{
				if (!coms.hasOwnProperty(comKey))
				{
					continue;
				}
				
				var command = coms[comKey];
				
				for (var minRoleKey in minRoles)
				{
					if (!minRoles.hasOwnProperty(minRoleKey))
					{
						continue;
					}
					
					if (!command.aliases.includes(minRoleKey.toLowerCase()))
					{
						continue;
					}
					
					// Bind result to new variable as a callback will change its result because minRoleKey gets updated in this loop, not redefined.
					const newMinRole = getRole(minRoles[minRoleKey]);
					
					// If the role exists, set the new callback.
					if (newMinRole)
					{
						// Has to be a callback for consistency.
						command.getMinRole = () => newMinRole;
					}
					
					// Break out of minRole loop, continues next in command loop.
					break;
				}
			}
		}
		
		self.onMessage = function(msg)
		{
			const content = msg.content;
			var spaceIndex = content.indexOf(" ");
		
			// If there are no spaces, the command should be the whole message.
			if (spaceIndex == -1)
			{
				spaceIndex = content.length;
			}
			
			var inCommand = content.substr(0, spaceIndex).toLowerCase();
			
			if (inCommand.startsWith(commandPrefix))
			{
				// +1 to not include the space.  Needs to have lower and upper case for URLs.
				const comInput = content.substr(spaceIndex+1);
				
				inCommand = inCommand.substr(commandPrefix.length);
				
				for (var comKey in	coms)
				{
					if (!coms.hasOwnProperty(comKey))
					{
						continue;
					}
					
					const command = coms[comKey];
					
					if (!command.aliases.includes(inCommand))
					{
						continue;
					}
					
					// If role is sufficient.
					if (msg.member.highestRole.comparePositionTo(command.getMinRole()) >= 0)
					{
						command.func(new Input(comInput, msg));
					}
					
					return;
				}
				
				return;
			}
			
			const openQuery = openQueries[msg.author.id];
			
			if (openQuery)
			{
				// It is being handled so cancel the timout response.
				clearTimeout(openQuery.timer);
				
				// Pass in whole text.
				openQuery.func(new Input(content, msg), openQuery.input);
				
				return;
			}
		}
		
		function getRole(roleName)
		{
			roles = Discord.getGuild().roles;
			
			for (var role of roles.values())
			{
				if (role.name.toLowerCase() === roleName.toLowerCase())
				{
					return role;
				}
			}
		}
		
		function getDefRole()
		{
			return Discord.getGuild().defaultRole;
		}
		
		function getHighestRole()
		{
			const roles = Discord.getGuild().roles;
			var highestRole = null;
			
			for (var role of roles.values())
			{
				if (!highestRole)
				{
					highestRole = role;
					continue;
				}
				
				if (role.position > highestRole.position)
				{
					highestRole = role;
				}
			}
			
			return highestRole;
		}
		
		function comSearch(input)
		{
			if (!input.comInput)
			{
				input.msg.channel.send("Provide a search term.");
				return;
			}
			
			const searchTerm = input.comInput;
			
			videoSearch(searchTerm, 12, input.msg, (videos, msg) => searchResult(videos, searchTerm, msg));
		}
		
		function searchResult(videos, searchTerm, msg)
		{
			const maxLength = 50;
			
			try
			{
				var searchEmbed = new Discord.RichEmbed()
					.setColor(embedConfig.col)
					.setAuthor(embedConfig.restrictLength(Discord.getNick(msg.member) + "'s Search", maxLength), msg.member.user.avatarURL)
					.setDescription(embedConfig.restrictLength("***" + searchTerm + "***", maxLength, true))
					.setFooter("Type the number of the song you want, or type any non-number to cancel.");
				
				for (var i = 0; i < videos.length; ++i)
				{
					searchEmbed = Video.addVideoField(searchEmbed, i+1, videos.length, videos[i]);
				}
				
				msg.channel.send(searchEmbed);
			}
			catch (err)
			{
				console.log("Couldn't send search embed: " + err);
			}
			
			openQueries[msg.author.id] = {func: queryResponseSearch, input: videos, timer: setTimeout(() => searchTimeout(msg), 30000)};
		}
		
		function searchTimeout(msg)
		{
			delete openQueries[msg.author.id];
			
			msg.channel.send(Discord.getUserName(msg.author.id) + "'s search was not responded to in time.");
		}
		
		function queryResponseSearch(input, videos)
		{
			delete openQueries[input.msg.author.id];
			channel = input.msg.channel;
			
			if (!input.comInput)
			{
				cancelSearch();
				return;
			}
			
			const selectNum = parseInt(input.comInput, 10);
			
			if (!selectNum && selectNum !== 0)
			{
				cancelSearch();
				return;
			}
			
			if (selectNum < 1)
			{
				channel.send("Selection is too low.");
				cancelSearch();
				return;
			}
			
			if (selectNum > videos.length)
			{
				channel.send("Selection is too high (max " + videos.length + ").");
				cancelSearch();
				return;
			}
			
			player.addSong(videos[selectNum-1], input.msg);
			
			function cancelSearch()
			{
				channel.send("Search cancelled.");
			}
		}
	}
})();

module.exports = InputHandler;